def tiempo(packet):
    try:
        import time

        if  packet["RTP"]["payload"]:
            now = round(time.time() * 1000)
            global tiempo

            try:
                print("tiempo = ", now - tiempo)
            except:
                pass
            tiempo = now

    except:
        pass
    return packet
